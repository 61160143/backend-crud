-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: sample
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `incorrectanswers`
--

DROP TABLE IF EXISTS `incorrectanswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `incorrectanswers` (
  `incorrect_answers_id` int NOT NULL AUTO_INCREMENT,
  `incorrect_answers` varchar(200) NOT NULL,
  `question_id` int NOT NULL,
  PRIMARY KEY (`incorrect_answers_id`),
  KEY `question_id_idx` (`question_id`),
  CONSTRAINT `question_id` FOREIGN KEY (`question_id`) REFERENCES `quiz` (`question_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=1276 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incorrectanswers`
--

LOCK TABLES `incorrectanswers` WRITE;
/*!40000 ALTER TABLE `incorrectanswers` DISABLE KEYS */;
INSERT INTO `incorrectanswers` VALUES (1084,'Hammersmith',381),(1085,'Vauxhall ',381),(1086,'Battersea',381),(1087,'Benetton',382),(1088,'Ferrari',382),(1089,'Mercedes',382),(1090,'Nike',383),(1091,'Adidas',383),(1092,'Reebok',383),(1093,'Bye',384),(1094,'Beamer',384),(1095,'Carry',384),(1096,'New York Rangers',385),(1097,'Toronto Maple Leafs',385),(1098,'Boston Bruins',385),(1099,'Giant Haystacks',386),(1100,'Kendo Nagasaki',386),(1101,'Masambula',386),(1102,'Harlequins',387),(1103,'Saracens',387),(1104,'Wasps',387),(1105,' Football Pitch',388),(1106,'Cricket Outfield',388),(1107,'Pinball Table',388),(1108,'Ayrton Senna',389),(1109,'Niki Lauda',389),(1110,'Emerson Fittipaldi',389),(1111,'62',390),(1112,'42',390),(1113,'102',390),(1114,'Yellow',391),(1115,'Brown',391),(1116,'Blue',391),(1117,'United Kingdom',392),(1118,'Brazil',392),(1119,'South Africa',392),(1120,'The Showdown (Australia)',393),(1121,'Verstappen on Fire (Germany)',393),(1122,'Schumacher&#039;s Ban (Britain)',393),(1123,'White',394),(1124,'Red',394),(1125,'Green',394),(1126,'Uganda',395),(1127,'Vietnam',395),(1128,'Bolivia',395),(1129,'Golden State Warriors',396),(1130,'Toronto Raptors',396),(1131,'Oklahoma City Thunders',396),(1132,'Melbourne Storm',397),(1133,'Sydney Roosters',397),(1134,'North Queensland Cowboys',397),(1135,'Toyota',398),(1136,'Audi',398),(1137,'Ferrari',398),(1138,'Argentina',399),(1139,'Brazil',399),(1140,'Paraguay',399),(1141,'Argentina',400),(1142,'Brazil',400),(1143,'Colombia',400),(1144,'France',401),(1145,'Germany',401),(1146,'England',401),(1147,'Scuderia Ferrari',402),(1148,'McLaren Honda',402),(1149,'Red Bull Racing Renault',402),(1150,'Chicago Cubs',403),(1151,'Cincinnati Reds',403),(1152,'St. Louis Cardinals',403),(1153,'Ayrton Senna',404),(1154,'Ronald Ratzenberger',404),(1155,'Gilles Villeneuve',404),(1156,'French Open',405),(1157,'Wimbledon',405),(1158,'Australian Open',405),(1159,'100m',406),(1160,'100yd',406),(1161,'109.36yd',406),(1162,'Terceira',407),(1163,'Santa Maria',407),(1164,'Porto Santo',407),(1165,'Barcelona',408),(1166,'Bayern Munich',408),(1167,'Liverpool',408),(1168,'Alabama Crimson Tide',409),(1169,'Clemson Tigers',409),(1170,'Wisconsin Badgers',409),(1171,'Jacob deGrom',410),(1172,'Shelby Miller',410),(1173,'Matt Harvey',410),(1174,'To make getting home runs harder.',411),(1175,'To display advertisements.',411),(1176,'To provide extra seating.',411),(1177,'Wrestling',412),(1178,'Archery',412),(1179,'Horse-Racing',412),(1180,'Toyota',413),(1181,'Audi',413),(1182,'Chevrolet',413),(1183,'Allen Iverson',414),(1184,'Kobe Bryant',414),(1185,'Paul Pierce',414),(1186,'Swimming',415),(1187,'Showjumping',415),(1188,'Gymnastics',415),(1189,'4',416),(1190,'9',416),(1191,'2',416),(1192,'Sir Bobby Charlton',417),(1193,'Ryan Giggs',417),(1194,'David Beckham',417),(1195,'Cristiano Ronaldo',418),(1196,'Robin Van Persie',418),(1197,'David Beckham',418),(1198,'Chris Benoit',419),(1199,'Lex Luger',419),(1200,'Al Snow',419),(1201,'David Beckham',420),(1202,'Steven Gerrard',420),(1203,'Michael Owen',420),(1204,'Daniil Kvyat',421),(1205,'Jolyon Palmer',421),(1206,'Rio Haryanto',421),(1207,'65',422),(1208,'63',422),(1209,'67',422),(1213,'Toronto Rock',424),(1214,'Toronto Argonauts',424),(1215,'Toronto Wolfpack',424),(1216,'Canada',425),(1217,'United States',425),(1218,'Germany',425),(1219,'Kareem Abdul-Jabbar',426),(1220,'Kevin Garnett',426),(1221,'Kobe Bryant',426),(1222,'Boston Celtics',427),(1223,'Philadelphia 76ers',427),(1224,'Golden State Warriors',427),(1225,'United States of America',428),(1226,'Italy',428),(1227,'Netherlands',428),(1228,'Sebastian Vettel',429),(1229,'Kimi Raikkonen',429),(1230,'Lewis Hamilton',429),(1231,'Thomas M&uuml;ller',430),(1232,'Lionel Messi',430),(1233,'Neymar',430),(1234,'Cleveland Cavaliers',431),(1235,'Houston Rockets',431),(1236,'Atlanta Hawks',431),(1237,'Chicago Bears',432),(1238,'Green Bay Packers',432),(1239,'New York Giants',432),(1240,'1965',433),(1241,'1959',433),(1242,'1963',433),(1243,'11',434),(1244,'20',434),(1245,'22',434),(1246,'Australia',435),(1247,'Turkey',435),(1248,'Cambodia',435),(1266,'cat',441),(1269,'fox',441),(1270,'dog',441);
/*!40000 ALTER TABLE `incorrectanswers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 22:40:16
