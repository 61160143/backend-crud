-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: sample
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `quiz`
--

DROP TABLE IF EXISTS `quiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `quiz` (
  `question_id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `question` varchar(400) NOT NULL,
  `difficulty` varchar(45) NOT NULL,
  `correct_answer` varchar(200) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=442 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quiz`
--

LOCK TABLES `quiz` WRITE;
/*!40000 ALTER TABLE `quiz` DISABLE KEYS */;
INSERT INTO `quiz` VALUES (381,'Sports','multiple','At which bridge does the annual Oxford and Cambridge boat race start?','medium','Putney'),(382,'Sports','multiple','With which team did Michael Schumacher make his Formula One debut at the 1991 Belgian Grand Prix?','medium','Jordan'),(383,'Sports','multiple','Which German sportswear company&#039;s logo is the &#039;Formstripe&#039;?','medium','Puma'),(384,'Sports','multiple','What cricketing term denotes a batsman being dismissed with a score of zero?','medium','Duck'),(385,'Sports','multiple','Which of these teams isn&#039;t a member of the NHL&#039;s &quot;Original Six&quot; era?','medium','Philadelphia Flyers'),(386,'Sports','multiple','Who was the British professional wrestler Shirley Crabtree better known as?','medium','Big Daddy'),(387,'Sports','multiple','What is the nickname of Northampton town&#039;s rugby union club?','medium','Saints'),(388,'Sports','multiple','A stimpmeter measures the speed of a ball over what surface?','medium','Golf Putting Green'),(389,'Sports','multiple','Which Formula One driver was nicknamed &#039;The Professor&#039;?','medium','Alain Prost'),(390,'Sports','multiple','How many scoring zones are there on a conventional dart board?','medium','82'),(391,'Sports','multiple','In a game of snooker, what colour ball is worth 3 points?','medium','Green'),(392,'Sports','multiple','Which nation hosted the FIFA World Cup in 2006?','medium','Germany'),(393,'Sports','multiple','The F1 season of 1994 is remembered for what tragic event?','medium','Death of Ayrton Senna (San Marino)'),(394,'Sports','multiple','What is the highest belt you can get in Taekwondo?','medium','Black'),(395,'Sports','multiple','Which country is hosting the 2022 FIFA World Cup?','medium','Qatar'),(396,'Sports','multiple','Which team was the 2015-2016 NBA Champions?','medium','Cleveland Cavaliers'),(397,'Sports','multiple','Josh Mansour is part of what NRL team?','medium','Penrith Panthers'),(398,'Sports','multiple','Which car manufacturer won the 2016 24 Hours of Le Mans?','medium','Porsche'),(399,'Sports','multiple','Which soccer team won the Copa Am&eacute;rica 2015 Championship ?','medium','Chile'),(400,'Sports','multiple','Which soccer team won the Copa Am&eacute;rica Centenario 2016?','medium','Chile'),(401,'Sports','multiple','What national team won the 2016 edition of UEFA European Championship?','medium','Portugal'),(402,'Sports','multiple','In 2016, who won the Formula 1 World Constructor&#039;s Championship for the third time in a row?','medium','Mercedes-AMG Petronas'),(403,'Sports','multiple','What is the oldest team in Major League Baseball?','medium','Atlanta Braves'),(404,'Sports','multiple','In Formula 1, the Virtual Safety Car was introduced following the fatal crash of which driver?','medium','Jules Bianchi'),(405,'Sports','multiple','Which of the following Grand Slam tennis tournaments occurs LAST?','medium','US Open'),(406,'Sports','multiple','What is the exact length of one non-curved part in Lane 1 of an Olympic Track?','medium','84.39m'),(407,'Sports','multiple','Which portuguese island is soccer player Cristiano Ronaldo from?','medium','Madeira'),(408,'Sports','multiple','Who won the &quot;Champions League&quot; in 1999?','medium','Manchester United'),(409,'Sports','multiple','Who won the 2015 College Football Playoff (CFP) National Championship? ','medium','Ohio State Buckeyes'),(410,'Sports','multiple','Which of the following pitchers was named National League Rookie of the Year for the 2013 season?','medium','Jose Fernandez'),(411,'Sports','multiple','Why was The Green Monster at Fenway Park was originally built?','medium','To prevent viewing games from outside the park.'),(412,'Sports','multiple','Which sport is NOT traditionally played during the Mongolian Naadam festival?','medium','American Football'),(413,'Sports','multiple','Which car manufacturer won the 2017 24 Hours of Le Mans?','medium','Porsche'),(414,'Sports','multiple','Which NBA player won Most Valuable Player for the 1999-2000 season?','medium','Shaquille O&#039;Neal'),(415,'Sports','multiple','In what sport does Fanny Chmelar compete for Germany?','medium','Skiing'),(416,'Sports','multiple','How many French Open&#039;s did Bj&ouml;rn Borg win?','medium','6'),(417,'Sports','multiple','Who is Manchester United&#039;s top premier league goal scorer?','medium','Wayne Rooney'),(418,'Sports','multiple','Which of the following player scored a hat-trick during their Manchester United debut?','medium','Wayne Rooney'),(419,'Sports','multiple','Which professional wrestler fell from the rafters to his death during a live Pay-Per-View event in 1999?','medium','Owen Hart'),(420,'Sports','multiple','Who was the topscorer for England national football team?','medium','Wayne Rooney'),(421,'Sports','multiple','Which Formula 1 driver switched teams in the middle of the 2017 season?','medium','Carlos Sainz Jr.'),(422,'Sports','multiple','What is Tiger Woods&#039; all-time best career golf-score?','medium','61'),(423,'Cartoon','multiple','What was Tom\'s full name?','medium','Thomas Tom'),(424,'Sports','multiple','What is the name of the AHL affiliate of the Toronto Maple Leafs?','medium','Toronto Marlies'),(425,'Sports','multiple','What country hosted the 2014 Winter Olympics?','medium','Russia'),(426,'Sports','multiple','Which NBA player has the most games played over the course of their career?','medium','Robert Parish'),(427,'Sports','multiple','Which basketball team has attended the most NBA grand finals?','medium','Los Angeles Lakers'),(428,'Sports','multiple','Which of these countries&#039; national teams qualified for the 2018 FIFA World Cup in Russia?','medium','Tunisia'),(429,'Sports','multiple','Who won the 2018 Monaco Grand Prix?','medium','Daniel Ricciardo'),(430,'Sports','multiple','Who was the top scorer of the 2014 FIFA World Cup?','medium','James Rodr&iacute;guez'),(431,'Sports','multiple','Which team was the 2014-2015 NBA Champions?','medium','Golden State Warriors'),(432,'Sports','multiple','What is the oldest team in the NFL?','medium','Arizona Cardinals'),(433,'Sports','multiple','What year was hockey legend Wayne Gretzky born?','medium','1961'),(434,'Sports','multiple','How many premier league trophies did Sir Alex Ferguson win during his time at Manchester United?','medium','13'),(435,'Sports','multiple','Which country did Kabaddi, a contact sport involving tackling, originate from?','medium','India'),(441,'Cartoon','multiple','What was Tom\'s full name?','medium','Thomas Tom');
/*!40000 ALTER TABLE `quiz` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-13 22:40:16
