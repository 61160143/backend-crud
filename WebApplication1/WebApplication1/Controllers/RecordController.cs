﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;
using System.Net.Http.Json;
using System.Text.Json;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{

    public class RecordController : ControllerBase
    {
        readonly Config config;
        readonly SampleDbContext _context;
        readonly IHttpClientFactory _httpClientFactory;

        public RecordController(Config configInject, SampleDbContext context, IHttpClientFactory httpClientFactory)
        {
            config = configInject;
            _context = context;
            _httpClientFactory = httpClientFactory;

        }

        [HttpPost]
        public IActionResult CreateRecordPersons([FromBody] RecordModel[] data)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {

                    //it's add new recordpersons

                    foreach (var record in data)
                    {
                        recordperson data_record = new recordperson()
                        {
                            category = record.category,
                            recordperson_name = record.name,
                            recordperson_time = record.time_person
                        };
                        _context.recordpeople.Add(data_record);
                    }
                    
                    _context.SaveChanges();
                    transaction.Commit();
                    return Ok("Add new data succeed");




                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }

        [HttpGet]
        public IActionResult GetRecords()
        {
            //Console.WriteLine(data);
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var category = from p in _context.recordpeople
                                   group p.recordperson_id by p.category into g
                                   select new { catagory = g.Key, recordperson_id = g.ToList() };

                    int Index = 1;

                    RecordViewModel recordViewModel = new RecordViewModel();

                    var RecordAsc = (from s in _context.recordpeople
                                    orderby s.recordperson_time ascending
                                    select s).ToList();

                    var RecordDesc = (from a in _context.recordpeople
                                     orderby a.recordperson_time descending
                                     select a).ToList();

                   

                    foreach (var item in category)
                    {
                        RecordModels recordkeep = new RecordModels();

                        foreach (var record in RecordAsc)
                        {
                            if(item.catagory == record.category)
                            {
                                recordkeep.record_id = Index++;
                                recordkeep.category = item.catagory;
                                recordkeep.time_personfast = record.recordperson_time;
                                recordkeep.name_personfast = record.recordperson_name;
                                break;
                            }
                        }

                        foreach (var record in RecordDesc)
                        {
                            if (item.catagory == record.category)
                            {
                                recordkeep.name_personslow = record.recordperson_name;
                                recordkeep.time_personslow = record.recordperson_time;
                                break;
                            }
                        }

                        recordViewModel.results.Add(recordkeep);

                    }

                    return Ok(recordViewModel);

                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }



        //[HttpPost]
        //public IActionResult CreateRecords([FromBody] RecordModel data)
        //{
        //    using (var transaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {

        //            //start check duplicate
        //            var duplicateRecord = from p in _context.recordpeople where p.category == data.category select p.category;

        //            if (duplicateRecord.Count() > 0)
        //            {
        //                //if duplicate
        //                return Ok("New data is duplicate");
        //            }
        //            else
        //            {
        //                //it's add new type
        //                recordperson data_record = new recordperson()
        //                {
        //                    category = data.category,
        //                    name_personfast = data.name,
        //                    time_personfast = data.time_person,
        //                    name_personslow = data.name,
        //                    time_personslow = data.time_person
        //                };


        //                _context.recordpeople.Add(data_record);
        //                _context.SaveChanges();
        //                transaction.Commit();
        //                return Ok("Add new data succeed");
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            transaction.Rollback();
        //            return BadRequest(ex.Message);
        //        }
        //    }
        //}

    }
}
