﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;
using System.Net.Http.Json;
using System.Text.Json;
//using System.Web.Http;
//using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
//using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;

namespace WebApplication1.Controllers
{
    
    public class TestController : ControllerBase
    {
        readonly Config config;
        readonly SampleDbContext _context;
        readonly IHttpClientFactory _httpClientFactory;

        public TestController(Config configInject, SampleDbContext context, IHttpClientFactory httpClientFactory)
        {
            config = configInject;
            _context = context;
            _httpClientFactory = httpClientFactory;

        }

        [HttpGet]
        public IActionResult GetQuestionInfo(int id)
        {
            var questionList = (from q in _context.quizzes
                                where q.question_id == id
                                select q).ToList();

            return Ok(questionList);
        }


        [HttpGet]
        public IActionResult GetQuestionList()
        {
            var questionList = (from q in _context.quizzes
                               orderby q.question_id
                               select new { q.question_id, q.question}).ToList();
            
                            

            return Ok(questionList);
        }


        [HttpGet]
        public IActionResult AddQuestion()
        {
            return Ok(config.ProjectName);
        }


        [HttpGet]
        public IActionResult EditQuestion()
        {
            return Ok(config.ProjectName);
        }


        [HttpGet]
        public IActionResult DeleteQuestion()
        {
            return Ok(config.ProjectName);
        }


        [HttpGet]
        public IActionResult Index()
        {
            return Ok(config.ProjectName);
        }



        //[HttpGet]
        //public IActionResult Question()
        //{
        //    //return Ok(_context.quizzes);

        //    var query = (from q in _context.quizzes
        //                 where q.category == "History"
        //                 select q).ToList();
        //    return Ok(query);

        //}

        [HttpGet]
        public async Task<IActionResult> apiTest()
        {
            var url = "https://opentdb.com/api.php?amount=30&category=17&type=multiple";
            //var url = "https://opentdb.com/api.php?amount=30&category=21&difficulty=medium&type=multiple";
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, url);

            var httpClient = _httpClientFactory.CreateClient();
            var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                using var contentStream = await httpResponseMessage.Content.ReadAsStreamAsync();

                ApiModel result = await JsonSerializer.DeserializeAsync<ApiModel>(contentStream);

                CreateQuiz(result);

                Console.WriteLine(result);

            }

            return new JsonResult("");
        }

        [HttpGet]
        public IActionResult Questions(int amount, string category = "" ,string difficulty = "" , string type = ""  ,int choice = 4)
        {         
            //return Ok(_context.quizzes);

            var question = (from q in _context.quizzes
                            where (q.category == category || q.category == "") 
                            && (q.difficulty == difficulty || q.difficulty == "") 
                            && (q.type == type || q.type == "")
                            select q).Take(amount).ToList();

            ApiModel result_data = new ApiModel();

            foreach (var quiz in question) {

                QuizData resultModel = new QuizData();
                resultModel.question_id = quiz.question_id;
                resultModel.category = quiz.category;
                resultModel.type = quiz.type;
                resultModel.difficulty = quiz.difficulty;
                resultModel.question = quiz.question;
                resultModel.correct_answer = quiz.correct_answer;
                var incorrect_answers = (from x in _context.incorrectanswers
                                         where x.question_id == quiz.question_id
                                         select x.incorrect_answers).Take(choice-1).ToList();

                Console.WriteLine(incorrect_answers);


                foreach (var incorrectAw in incorrect_answers)
                {
                    resultModel.incorrect_answers.Add(incorrectAw);
                }

                result_data.results.Add(resultModel);
            }

            //var rnd = new Random();
            //var randomquestion = result_data.results.OrderBy(item => rnd.Next()).ToList();
            //result_data.results = randomquestion;

            return Ok(result_data);

        }


        void CreateQuiz(ApiModel data)
        {
            //Console.WriteLine(data);
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < data.results.Count; i++)
                    {

                        var result = data.results[i];
                        //start check duplicate
                        var duplicateQuestion = from p in _context.quizzes where p.question == result.question select p.question;

                        if (duplicateQuestion.Count() > 0)
                        {
                            //if duplicate
                            Console.WriteLine("New data is duplicate");
                        }
                        else
                        {
                            //it's add new question
                            quiz data_question = new quiz()
                            {
                                category = result.category,
                                difficulty = result.difficulty,
                                type = result.type,
                                question = result.question,
                                correct_answer = result.correct_answer
                            };
                            _context.Add(data_question);
                            _context.SaveChanges();

                            int id = data_question.question_id;

                            for (var j = 0; j < result.incorrect_answers.Count; j++)
                            {
                                //AddIncorrectAw(result.incorrect_answers[j], id);
                                incorrectanswer data_incorrectanswer = new incorrectanswer()
                                {
                                    incorrect_answers = result.incorrect_answers[j],
                                    question_id = id
                                };
                                _context.Add(data_incorrectanswer);

                                _context.SaveChanges();
                            }

                        }
                    }

                    transaction.Commit();

                }catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }

        //try
        //{

        //    quiz test = new quiz()
        //    {
        //        category = "Sports",
        //        difficulty = "medium",
        //        type = "multiple",
        //        question = "At which bridge does the annual Oxford and Cambridge boat race start?",
        //        correct_answer = "Putney"

        //    };
        //    ApiModel quiz = new ApiModel() { };
        //    context.Add(test);


        //    //saves all above operations within one transaction    
        //    context.SaveChanges();
        //    Console.WriteLine(test);
        //    int id = test.question_id;

        //    AddIncorrectAw("Hammersmith", id);
        //    AddIncorrectAw("Vauxhall", id);
        //    AddIncorrectAw("Battersea", id);
        //    //commit transaction    
        //    //dbContextTransaction.Commit();
        //}
        //catch (Exception ex)
        //{
        //    //Rollback transaction if exception occurs
        //    //dbContextTransaction.Rollback();
        //}


        //void AddIncorrectAw(String data, int id)
        //{
        //    //Console.WriteLine(data);

        //    try
        //    {
        //        incorrectanswer data_incorrectanswer = new incorrectanswer()
        //        {
        //            incorrect_answers = data,
        //            question_id = id

        //        };
        //        //ApiModel quiz = new ApiModel() {  };
        //        _context.Add(data_incorrectanswer);


        //        ////saves all above operations within one transaction    
        //        _context.SaveChanges();

        //        ////commit transaction    
        //        //dbContextTransaction.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Rollback transaction if exception occurs    
        //        //dbContextTransaction.Rollback();
        //    }

        //}

    }
}
