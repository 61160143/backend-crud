﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.DataBase.Context;
using WebApplication1.DataBase.Models;
using System.Net.Http.Json;
using System.Text.Json;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{

    public class QuestionController : ControllerBase
    {
        readonly Config config;
        readonly SampleDbContext _context;
        readonly IHttpClientFactory _httpClientFactory;

        public QuestionController(Config configInject, SampleDbContext context, IHttpClientFactory httpClientFactory)
        {
            config = configInject;
            _context = context;
            _httpClientFactory = httpClientFactory;

        }

        [HttpGet]
        public IActionResult GetQuestionInfo(int id)
        {
            var question = (from q in _context.quizzes
                            where q.question_id == id
                            select q).FirstOrDefault();

            if (question != null)
            {
                QuizDataViewModels resultModel = new QuizDataViewModels();
                resultModel.question_id = question.question_id;
                resultModel.category = question.category;
                resultModel.type = question.type;
                resultModel.difficulty = question.difficulty;
                resultModel.question = question.question;
                resultModel.correct_answer = question.correct_answer;
                resultModel.incorrect_answers = (from x in _context.incorrectanswers
                                                 where x.question_id == question.question_id
                                                 select new IncorrectAnswer
                                                 {
                                                     incorrect_answers_id = x.incorrect_answers_id,
                                                     incorrect_answers = x.incorrect_answers
                                                 }).ToList();


                return Ok(resultModel);
            }
            else
            {
                return Ok("");
            }




        }


        //[HttpGet]
        //public IActionResult GetQuestionList(string category, string difficulty, string type, string question)
        //{
        //    var questionList = (from q in _context.quizzes
        //                        orderby q.question_id
        //                        where (q.category == category || string.IsNullOrEmpty(category))
        //                              && (q.difficulty == difficulty || string.IsNullOrEmpty(difficulty))
        //                              && (q.type == type || string.IsNullOrEmpty(type))
        //                              && (q.question.Contains(question) || string.IsNullOrEmpty(question))
        //                        select new { q.question_id, q.category, q.difficulty, q.type, q.question }).ToList();

        //    return new JsonResult(questionList);
        //}

        [HttpGet]
        public IActionResult GetQuestionList([FromQuery] QuestionListModel model)
        {
            var questionList = (from q in _context.quizzes
                                orderby q.question_id
                                where (q.category == model.category || string.IsNullOrEmpty(model.category))
                                      && (q.difficulty == model.difficulty || string.IsNullOrEmpty(model.difficulty))
                                      && (q.type == model.type || string.IsNullOrEmpty(model.type))
                                      && (q.question.Contains(model.question) || string.IsNullOrEmpty(model.question))
                                select new { q.question_id, q.category, q.difficulty, q.type, q.question }).ToList();

            return new JsonResult(questionList);
        }


        [HttpPost]
        public IActionResult CreateQuestion([FromBody] QuizData data)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {

                    //start check duplicate
                    var duplicateQuestion = from p in _context.quizzes where p.question == data.question select p.question;

                    if (duplicateQuestion.Count() > 0)
                    {
                        //if duplicate
                        return Ok("New data is duplicate");
                    }
                    else
                    {
                        //it's add new question
                        quiz data_question = new quiz()
                        {
                            category = data.category,
                            difficulty = data.difficulty,
                            type = data.type,
                            question = data.question,
                            correct_answer = data.correct_answer
                        };
                        _context.quizzes.Add(data_question);
                        _context.SaveChanges();

                        int id = data_question.question_id;

                        foreach (var incorrectanswer in data.incorrect_answers)
                        {
                            incorrectanswer data_incorrectanswer = new incorrectanswer()
                            {
                                incorrect_answers = incorrectanswer,
                                question_id = id
                            };
                            _context.incorrectanswers.Add(data_incorrectanswer);
                        }
                        _context.SaveChanges();
                        transaction.Commit();
                        return Ok("Add new data succeed ID : " + id);
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }


        [HttpPost] // fix ModifyQuestion
        public IActionResult ModifyQuestion([FromBody] QuizDataViewModels data)
        {
            if(!ModelState.IsValid)
                return BadRequest(ModelState);

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {

                    var updateQuestion = _context.quizzes.Single(x => x.question_id == data.question_id);


                    if (updateQuestion == null)
                    {
                        return Ok("This id no have in database");
                    }
                    else
                    {

                        //updateQuestion.category = data.category != null ? data.category : updateQuestion.category;

                        //updateQuestion.type = data.type != null ? data.type : updateQuestion.type;

                        //updateQuestion.question = data.question != null ? data.question : updateQuestion.question;

                        //updateQuestion.difficulty = data.difficulty != null ? data.difficulty : updateQuestion.difficulty;

                        //updateQuestion.correct_answer = data.correct_answer != null ? data.correct_answer : updateQuestion.correct_answer;

                        updateQuestion.category = data.category;

                        updateQuestion.type = data.type;

                        updateQuestion.question = data.question;

                        updateQuestion.difficulty = data.difficulty;

                        updateQuestion.correct_answer = data.correct_answer;

                        var updateIncorrectAw = (from x in _context.incorrectanswers
                                                 where x.question_id == data.question_id
                                                 select x).ToList();

                        if (data.incorrect_answers.Any())
                        {
                            List<incorrectanswer> AddNewIncorect = new List<incorrectanswer>();
                            List<incorrectanswer> update = new List<incorrectanswer>();
                            List<incorrectanswer> delete = new List<incorrectanswer>(updateIncorrectAw);

                            foreach (var itemIncorrectAw in data.incorrect_answers)
                            {
                                if (itemIncorrectAw.incorrect_answers_id == 0)
                                {
                                    AddNewIncorect.Add(new incorrectanswer
                                    {
                                        incorrect_answers = itemIncorrectAw.incorrect_answers,
                                        question_id = data.question_id
                                    });
                                }
                                else
                                {
                                    var itemFromDatabaseIndex = updateIncorrectAw.FindIndex(x => x.incorrect_answers_id == itemIncorrectAw.incorrect_answers_id);
                                    var itemFromDatabase = updateIncorrectAw[itemFromDatabaseIndex];
                                    itemFromDatabase.incorrect_answers = itemIncorrectAw.incorrect_answers;

                                    update.Add(itemFromDatabase);
                                    delete.Remove(itemFromDatabase);


                                }
                            }

                            //var itemFromDatabaseIndex2 = FindIndex(updateitemincorrectAw.incorrect_answers_id, updateIncorrectAw);
                            //var itemFromDatabase2 = updateIncorrectAw[itemFromDatabaseIndex2];

                            //foreach (var updateitemincorrectAw in update)
                            //{
                            //    var itemFromDatabaseIndex2 = updateIncorrectAw.FindIndex(x => x.incorrect_answers_id == updateitemincorrectAw.incorrect_answers_id);
                            //    //var itemFromDatabaseIndex2 = FindIndex(updateitemincorrectAw.incorrect_answers_id, updateIncorrectAw);
                            //    var itemFromDatabase2 = updateIncorrectAw[itemFromDatabaseIndex2];

                            //    itemFromDatabase2.incorrect_answers = updateitemincorrectAw.incorrect_answers;

                            //}

                            _context.SaveChanges();
                            _context.incorrectanswers.RemoveRange(delete);
                            _context.incorrectanswers.AddRange(AddNewIncorect);

                        }





                        _context.SaveChanges();
                        transaction.Commit();
                        return Ok("Update new data succeed ID : ");

                        int FindIndex(int id, List<incorrectanswer> arr)
                        {

                            for (var i = 0; i < arr.Count; i++)
                            {
                                if (arr[i].incorrect_answers_id == id)
                                {
                                    return i;
                                }
                            }
                            return -1;
                        }
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }


        [HttpPost]
        public IActionResult DeleteQuestion([FromBody] int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var deleteQuestion = _context.quizzes.SingleOrDefault(x => x.question_id == id);


                    if (deleteQuestion == null)
                    {
                        return Ok("This id no have in database");
                    }
                    else
                    {
                        var deleteIncorrectAw = (from x in _context.incorrectanswers
                                                 where x.question_id == id
                                                 select x).ToList();


                        if (deleteIncorrectAw.Count > 0)
                        {
                            _context.incorrectanswers.RemoveRange(deleteIncorrectAw);
                        }
                        _context.quizzes.Remove(deleteQuestion);
                        _context.SaveChanges();
                        transaction.Commit();
                        return Ok("Delete data succeed");
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex.Message);
                }
            }
        }





        //[HttpGet]
        //public IActionResult Question()
        //{
        //    //return Ok(_context.quizzes);

        //    var query = (from q in _context.quizzes
        //                 where q.category == "History"
        //                 select q).ToList();
        //    return Ok(query);

        //}

        //[HttpGet]
        //public async Task<IActionResult> apiTest()
        //{
        //    //var url = "https://opentdb.com/api.php?amount=10&type=multiple";
        //    var url = "https://opentdb.com/api.php?amount=30&category=21&difficulty=medium&type=multiple";
        //    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, url);

        //    var httpClient = _httpClientFactory.CreateClient();
        //    var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

        //    if (httpResponseMessage.IsSuccessStatusCode)
        //    {
        //        using var contentStream = await httpResponseMessage.Content.ReadAsStreamAsync();

        //        ApiModel result = await JsonSerializer.DeserializeAsync<ApiModel>(contentStream);

        //        CreateQuiz(result);

        //        Console.WriteLine(result);

        //    }

        //    return new JsonResult("");
        //}

        //[HttpGet]
        //public IActionResult Questions(int amount, string category = "" ,string difficulty = "" , string type = ""  ,int choice = 4)
        //{         
        //    //return Ok(_context.quizzes);

        //    var question = (from q in _context.quizzes
        //                    where (q.category == category || q.category == "") 
        //                    || (q.difficulty == difficulty || q.difficulty == "") 
        //                    || (q.type == type || q.type == "")
        //                    select q).Take(amount).ToList();

        //    ApiModel result_data = new ApiModel();

        //    foreach (var quiz in question) {

        //        QuizData resultModel = new QuizData();
        //        resultModel.question_id = quiz.question_id;
        //        resultModel.category = quiz.category;
        //        resultModel.type = quiz.type;
        //        resultModel.difficulty = quiz.difficulty;
        //        resultModel.question = quiz.question;
        //        resultModel.correct_answer = quiz.correct_answer;
        //        var incorrect_answers = (from x in _context.incorrectanswers
        //                                 where x.question_id == quiz.question_id
        //                                 select x.incorrect_answers).Take(choice-1).ToList();

        //        foreach (var incorrectAw in incorrect_answers)
        //        {
        //            resultModel.incorrect_answers.Add(incorrectAw);
        //        }

        //        result_data.results.Add(resultModel);
        //    }

        //    var rnd = new Random();
        //    var randomquestion = result_data.results.OrderBy(item => rnd.Next()).ToList();
        //    result_data.results = randomquestion;


        //    return Ok(result_data.results);

        //}


        void CreateQuiz(ApiModel data)
        {
            //Console.WriteLine(data);
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < data.results.Count; i++)
                    {

                        var result = data.results[i];
                        //start check duplicate
                        var duplicateQuestion = from p in _context.quizzes where p.question == result.question select p.question;

                        if (duplicateQuestion.Count() > 0)
                        {
                            //if duplicate
                            Console.WriteLine("New data is duplicate");
                        }
                        else
                        {
                            //it's add new question
                            quiz data_question = new quiz()
                            {
                                category = result.category,
                                difficulty = result.difficulty,
                                type = result.type,
                                question = result.question,
                                correct_answer = result.correct_answer
                            };
                            _context.Add(data_question);
                            _context.SaveChanges();

                            int id = data_question.question_id;

                            for (var j = 0; j < result.incorrect_answers.Count; j++)
                            {
                                //AddIncorrectAw(result.incorrect_answers[j], id);
                                incorrectanswer data_incorrectanswer = new incorrectanswer()
                                {
                                    incorrect_answers = result.incorrect_answers[j],
                                    question_id = id
                                };
                                _context.Add(data_incorrectanswer);

                                _context.SaveChanges();
                            }

                        }
                    }

                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }

        //try
        //{

        //    quiz test = new quiz()
        //    {
        //        category = "Sports",
        //        difficulty = "medium",
        //        type = "multiple",
        //        question = "At which bridge does the annual Oxford and Cambridge boat race start?",
        //        correct_answer = "Putney"

        //    };
        //    ApiModel quiz = new ApiModel() { };
        //    context.Add(test);


        //    //saves all above operations within one transaction    
        //    context.SaveChanges();
        //    Console.WriteLine(test);
        //    int id = test.question_id;

        //    AddIncorrectAw("Hammersmith", id);
        //    AddIncorrectAw("Vauxhall", id);
        //    AddIncorrectAw("Battersea", id);
        //    //commit transaction    
        //    //dbContextTransaction.Commit();
        //}
        //catch (Exception ex)
        //{
        //    //Rollback transaction if exception occurs
        //    //dbContextTransaction.Rollback();
        //}


        //void AddIncorrectAw(String data, int id)
        //{
        //    //Console.WriteLine(data);

        //    try
        //    {
        //        incorrectanswer data_incorrectanswer = new incorrectanswer()
        //        {
        //            incorrect_answers = data,
        //            question_id = id

        //        };
        //        //ApiModel quiz = new ApiModel() {  };
        //        _context.Add(data_incorrectanswer);


        //        ////saves all above operations within one transaction    
        //        _context.SaveChanges();

        //        ////commit transaction    
        //        //dbContextTransaction.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        //Rollback transaction if exception occurs    
        //        //dbContextTransaction.Rollback();
        //    }

        //}

    }
}
