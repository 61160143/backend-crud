﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApplication1.DataBase.Models;

namespace WebApplication1.DataBase.Context
{
    public partial class SampleDbContext : DbContext
    {
        public SampleDbContext()
        {
        }

        public SampleDbContext(DbContextOptions<SampleDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<incorrectanswer> incorrectanswers { get; set; } = null!;
        public virtual DbSet<quiz> quizzes { get; set; } = null!;
        public virtual DbSet<record> records { get; set; } = null!;
        public virtual DbSet<recordperson> recordpeople { get; set; } = null!;
        public virtual DbSet<student> students { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=127.0.0.1;port=3306;uid=root;password=95219633Zz!;database=sample", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.27-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<incorrectanswer>(entity =>
            {
                entity.HasKey(e => e.incorrect_answers_id)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.question_id, "question_id_idx");

                entity.Property(e => e.incorrect_answers).HasMaxLength(200);

                entity.HasOne(d => d.question)
                    .WithMany(p => p.incorrectanswers)
                    .HasForeignKey(d => d.question_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("question_id");
            });

            modelBuilder.Entity<quiz>(entity =>
            {
                entity.HasKey(e => e.question_id)
                    .HasName("PRIMARY");

                entity.ToTable("quiz");

                entity.Property(e => e.category).HasMaxLength(45);

                entity.Property(e => e.correct_answer).HasMaxLength(200);

                entity.Property(e => e.difficulty).HasMaxLength(45);

                entity.Property(e => e.question).HasMaxLength(400);

                entity.Property(e => e.type).HasMaxLength(45);
            });

            modelBuilder.Entity<record>(entity =>
            {
                entity.HasKey(e => e.record_id)
                    .HasName("PRIMARY");

                entity.ToTable("record");

                entity.Property(e => e.category).HasMaxLength(45);

                entity.Property(e => e.name_personfast).HasMaxLength(45);

                entity.Property(e => e.name_personslow).HasMaxLength(45);
            });

            modelBuilder.Entity<recordperson>(entity =>
            {
                entity.HasKey(e => e.recordperson_id)
                    .HasName("PRIMARY");

                entity.ToTable("recordperson");

                entity.Property(e => e.category).HasMaxLength(45);

                entity.Property(e => e.recordperson_name).HasMaxLength(45);
            });

            modelBuilder.Entity<student>(entity =>
            {
                entity.HasKey(e => e.student_id)
                    .HasName("PRIMARY");

                entity.ToTable("student");

                entity.Property(e => e.birthday).HasColumnType("datetime");

                entity.Property(e => e.fname).HasMaxLength(45);

                entity.Property(e => e.lname).HasMaxLength(45);

                entity.Property(e => e.student_code).HasMaxLength(45);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
