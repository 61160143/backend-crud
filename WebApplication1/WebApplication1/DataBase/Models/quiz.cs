﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class quiz
    {
        public quiz()
        {
            incorrectanswers = new HashSet<incorrectanswer>();
        }

        public int question_id { get; set; }
        public string category { get; set; } = null!;
        public string type { get; set; } = null!;
        public string question { get; set; } = null!;
        public string difficulty { get; set; } = null!;
        public string correct_answer { get; set; } = null!;

        public virtual ICollection<incorrectanswer> incorrectanswers { get; set; }
    }
}
