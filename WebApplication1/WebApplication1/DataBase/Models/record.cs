﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class record
    {
        public int record_id { get; set; }
        public string category { get; set; } = null!;
        public string name_personfast { get; set; } = null!;
        public double time_personfast { get; set; }
        public string name_personslow { get; set; } = null!;
        public double time_personslow { get; set; }
    }
}
