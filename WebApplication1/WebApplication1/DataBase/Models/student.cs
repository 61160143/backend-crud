﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class student
    {
        public int student_id { get; set; }
        public string student_code { get; set; } = null!;
        public string fname { get; set; } = null!;
        public string lname { get; set; } = null!;
        public DateTime birthday { get; set; }
    }
}
