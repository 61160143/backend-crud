﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public class ApiModel
    {
        public ApiModel()
        {
            results = new List<QuizData>();
        }
        public List<QuizData> results { get; set; } = null!;
    }

    public class QuizData
    {
        public QuizData()
        {
            incorrect_answers = new List<string>();

        }
        public int question_id { get; set; }
        public string category { get; set; } = null!;
        public string type { get; set; } = null!;
        public string question { get; set; } = null!;
        public string difficulty { get; set; } = null!;
        public string correct_answer { get; set; } = null!;
        public List<string> incorrect_answers { get; set; } = null!;
    }

    //public class IncorrectModel
    //{
    //    public IncorrectModel()
    //    {
    //        resultss = new List<incorrectanswer>();
    //    }
    //    public List<incorrectanswer> resultss { get; set; } = null!;
    //}
}
