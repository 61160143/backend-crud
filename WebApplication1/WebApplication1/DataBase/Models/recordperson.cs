﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class recordperson
    {
        public int recordperson_id { get; set; }
        public string category { get; set; } = null!;
        public string recordperson_name { get; set; } = null!;
        public double recordperson_time { get; set; }
    }
}
