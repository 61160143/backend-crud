﻿using System;
using System.Collections.Generic;

namespace WebApplication1.DataBase.Models
{
    public partial class incorrectanswer
    {
        public int incorrect_answers_id { get; set; }
        public string incorrect_answers { get; set; } = null!;
        public int question_id { get; set; }

        public virtual quiz question { get; set; } = null!;
    }
}
