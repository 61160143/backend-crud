﻿namespace WebApplication1.ViewModels
{
    public class QuizDataViewModels
    {   
        public int question_id { get; set; }
        public string category { get; set; } 
        public string type { get; set; } 
        public string question { get; set; } 
        public string difficulty { get; set; } 
        public string correct_answer { get; set; } 
        public List<IncorrectAnswer> incorrect_answers { get; set; } = new List<IncorrectAnswer>();
    }
    public class IncorrectAnswer
    {
        public int incorrect_answers_id { get; set; }
        public string incorrect_answers { get; set; }
    }

    public class QuestionListModel
    {
        public string category { get; set; }

        public string difficulty { get; set; }

        public string type { get; set; }

        public string question { get; set; }
    }
}
