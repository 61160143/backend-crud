﻿namespace WebApplication1.ViewModels
{
    public class RecordModels
    {
        public int record_id { get; set; }
        public string category { get; set; } = null!;
        public string name_personfast { get; set; } = null!;
        public double time_personfast { get; set; }
        public string name_personslow { get; set; } = null!;
        public double time_personslow { get; set; }
    }
    public class RecordViewModel{
         public RecordViewModel()
            {
                results = new List<RecordModels>();
            }
        public List<RecordModels> results { get; set; } = null!;
    }
    public class RecordModel
    {

        public string category { get; set; }

        public string name { get; set; }

        public double time_person { get; set; }
    }
}
